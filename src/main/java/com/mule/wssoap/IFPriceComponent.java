package com.mule.wssoap;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IFPriceComponent {
    
    public String getPrice(@WebParam(name = "destination") String destination);
}
