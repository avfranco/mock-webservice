package com.mule.wssoap;

import javax.jws.WebService;

@WebService(endpointInterface = "com.mule.wssoap.IFPriceComponent", serviceName = "IFPriceComponent")
public class PriceComponentSoap extends PriceComponent implements IFPriceComponent {
    
}
