package jp.mufg.br.crk.ws.vo.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import jp.mufg.br.crk.ws.vo.RequestBondsPortfolioVO;

@XmlRootElement(name = "requestBondsPortfolioVO", namespace = "http://ws.crk.br.mufg.jp/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestBondsPortfolioVO", namespace = "http://ws.crk.br.mufg.jp/")
public class RequestBondsPortfolio {
    
    @XmlElement(name = "GerarPosicaoTitulosPublicosLivres", namespace = "")
    private RequestBondsPortfolioVO gerarPosicaoTitulosPublicosLivres;
    
    /**
     * 
     * @return
     *         returns RequestBondsPortfolioVO
     */
    public RequestBondsPortfolioVO getGerarPosicaoTitulosPublicosLivres() {
	return this.gerarPosicaoTitulosPublicosLivres;
    }
    
    /**
     * 
     * @param gerarPosicaoTitulosPublicosLivres
     *            the value for the gerarPosicaoTitulosPublicosLivres
     *            property
     */
    public void setGerarPosicaoTitulosPublicosLivres(RequestBondsPortfolioVO gerarPosicaoTitulosPublicosLivres) {
	this.gerarPosicaoTitulosPublicosLivres = gerarPosicaoTitulosPublicosLivres;
    }
}
