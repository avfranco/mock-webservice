package jp.mufg.br.crk.ws.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetPosicaoTitulosPublicosLivres", propOrder = { "posicaoEstrategia" })
@XmlRootElement(name = "RetPosicaoTitulosPublicosLivres")
public class ResponseBondsPortfolioVO {
    
    @XmlElement(name = "PosicaoEstrategia", required = true)
    protected List<PosicaoEstrategia> posicaoEstrategia;
    
    public List<PosicaoEstrategia> getPosicaoEstrategia() {
	if (posicaoEstrategia == null) {
	    posicaoEstrategia = new ArrayList<PosicaoEstrategia>();
	}
	return this.posicaoEstrategia;
    }
    
}
