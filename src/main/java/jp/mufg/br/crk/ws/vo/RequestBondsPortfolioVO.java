package jp.mufg.br.crk.ws.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.springframework.format.annotation.DateTimeFormat;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "entity", "grupoPapel", "codigoPapel", "vencimentoPapel", "finalidadeEstoque", "categoriaContabil" })
@XmlRootElement(name = "RequestBondsPortfolioVO")
public class RequestBondsPortfolioVO {
    
    @XmlElement(name = "Entity", required = true)
    protected String entity;
    
    @XmlElement(name = "GrupoPapel", required = true)
    protected String grupoPapel;
    
    @XmlElement(name = "CodigoPapel", required = true)
    protected String codigoPapel;
    
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @XmlElement(name = "VencimentoPapel", required = true)
    protected String vencimentoPapel;
    
    @XmlElement(name = "FinalidadeEstoque")
    protected String finalidadeEstoque;
    
    @XmlElement(name = "CategoriaContabil")
    protected String categoriaContabil;
    
    /**
     * Gets the value of the entity property.
     * 
     */
    public String getEntity() {
	return entity;
    }
    
    /**
     * Sets the value of the entity property.
     * 
     */
    public void setEntity(String value) {
	this.entity = value;
    }
    
    /**
     * Gets the value of the grupoPapel property.
     * 
     */
    public String getGrupoPapel() {
	return grupoPapel;
    }
    
    /**
     * Sets the value of the grupoPapel property.
     * 
     */
    public void setGrupoPapel(String value) {
	this.grupoPapel = value;
    }
    
    /**
     * Gets the value of the codigoPapel property.
     * 
     */
    public String getCodigoPapel() {
	return codigoPapel;
    }
    
    /**
     * Sets the value of the codigoPapel property.
     * 
     */
    public void setCodigoPapel(String value) {
	this.codigoPapel = value;
    }
    
    /**
     * Gets the value of the vencimentoPapel property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getVencimentoPapel() {
	return vencimentoPapel;
    }
    
    /**
     * Sets the value of the vencimentoPapel property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setVencimentoPapel(String value) {
	this.vencimentoPapel = value;
    }
    
    /**
     * Gets the value of the finalidadeEstoque property.
     * 
     * @return
     *         possible object is {@link Byte }
     * 
     */
    public String getFinalidadeEstoque() {
	return finalidadeEstoque;
    }
    
    /**
     * Sets the value of the finalidadeEstoque property.
     * 
     * @param value
     *            allowed object is {@link Byte }
     * 
     */
    public void setFinalidadeEstoque(String value) {
	this.finalidadeEstoque = value;
    }
    
    /**
     * Gets the value of the categoriaContabil property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getCategoriaContabil() {
	return categoriaContabil;
    }
    
    /**
     * Sets the value of the categoriaContabil property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCategoriaContabil(String value) {
	this.categoriaContabil = value;
    }
    
}
