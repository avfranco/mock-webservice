package jp.mufg.br.crk.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.jws.WebMethod;
import javax.jws.WebParam;

import jp.mufg.br.crk.ws.enm.CategoriaContabilEnum;
import jp.mufg.br.crk.ws.enm.FinalidadeEstoqueEnum;
import jp.mufg.br.crk.ws.vo.PosicaoEstrategia;
import jp.mufg.br.crk.ws.vo.RequestBondsPortfolioVO;
import jp.mufg.br.crk.ws.vo.ResponseBondsPortfolioVO;

public class CalypsoToCRKInterfaceImpl implements CalypsoToCRKInterface {
    
    @Override
    @WebMethod(action = "requestBondsPortifolio")
    public ResponseBondsPortfolioVO requestBondsPortfolio(@WebParam(name = "GerarPosicaoTitulosPublicosLivres") RequestBondsPortfolioVO requestVO) {
	
	ResponseBondsPortfolioVO responseBonsBondsPortfolioVO = new ResponseBondsPortfolioVO();
	PosicaoEstrategia posicaoEstrategia = new PosicaoEstrategia();
	
	if (requestVO.getCategoriaContabil().equals(CategoriaContabilEnum.NEGOCIACAO.getCod())) {
	    posicaoEstrategia.setNegociacao(generatorRandomNumber());
	} else if (requestVO.getCategoriaContabil().equals(CategoriaContabilEnum.DISPONIVEL_PARA_VENDA.getCod())) {
	    posicaoEstrategia.setDisponivelParaVenda(generatorRandomNumber());
	} else if (requestVO.getCategoriaContabil().equals(CategoriaContabilEnum.MANTIDOS_ATE_O_VENCIMENTO.getCod())) {
	    posicaoEstrategia.setMantidosAteVencimento(generatorRandomNumber());
	}
	
	List<FinalidadeEstoqueEnum> listaFinalidadeEstoque = populaEstoque();
	
	for (FinalidadeEstoqueEnum listaFinalidadeEstoqueEnum : listaFinalidadeEstoque) {
	    if (requestVO.getFinalidadeEstoque().contains(listaFinalidadeEstoqueEnum.getCod())) {
		posicaoEstrategia.setFinalidadeEstoque(listaFinalidadeEstoqueEnum.toString());
	    }
	    responseBonsBondsPortfolioVO.getPosicaoEstrategia().add(posicaoEstrategia);
	}
	
	return responseBonsBondsPortfolioVO;
    }
    
    private List<FinalidadeEstoqueEnum> populaEstoque() {
	List<FinalidadeEstoqueEnum> listaEstoque = new ArrayList<>();
	listaEstoque.add(FinalidadeEstoqueEnum.INVESTMENT);
	listaEstoque.add(FinalidadeEstoqueEnum.TRADING);
	listaEstoque.add(FinalidadeEstoqueEnum.BANKING);
	listaEstoque.add(FinalidadeEstoqueEnum.HEDGE);
	listaEstoque.add(FinalidadeEstoqueEnum.COMPULSORY);
	listaEstoque.add(FinalidadeEstoqueEnum.DEPOSIT);
	listaEstoque.add(FinalidadeEstoqueEnum.HEDGE_ACCOUNTING);
	
	return listaEstoque;
    }
    
    private int generatorRandomNumber() {
	Random rand = new Random();
	int randomNum = rand.nextInt(10000 - 1) + 1;
	
	return randomNum;
    }
}
