package jp.mufg.br.crk.ws.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "negociacao", "disponivelParaVenda", "mantidosAteVencimento", "finalidadeEstoque" })
public class PosicaoEstrategia {
    
    @XmlElement(name = "Negociacao", required = true)
    protected int    negociacao;
    @XmlElement(name = "DisponivelParaVenda", required = true)
    protected int    disponivelParaVenda;
    @XmlElement(name = "MantidosAteVencimento", required = true)
    protected int    mantidosAteVencimento;
    @XmlElement(name = "FinalidadeEstoque", required = true)
    protected String finalidadeEstoque;
    
    /**
     * Gets the value of the negociacao property.
     * 
     */
    public int getNegociacao() {
	return negociacao;
    }
    
    /**
     * Sets the value of the negociacao property.
     * 
     */
    public void setNegociacao(int value) {
	this.negociacao = value;
    }
    
    /**
     * Gets the value of the disponivelParaVenda property.
     * 
     */
    public int getDisponivelParaVenda() {
	return disponivelParaVenda;
    }
    
    /**
     * Sets the value of the disponivelParaVenda property.
     * 
     */
    public void setDisponivelParaVenda(int value) {
	this.disponivelParaVenda = value;
    }
    
    /**
     * Gets the value of the mantidosAteVencimento property.
     * 
     */
    public int getMantidosAteVencimento() {
	return mantidosAteVencimento;
    }
    
    /**
     * Sets the value of the mantidosAteVencimento property.
     * 
     */
    public void setMantidosAteVencimento(int value) {
	this.mantidosAteVencimento = value;
    }
    
    /**
     * Gets the value of the finalidadeEstoque property.
     * 
     */
    public String getFinalidadeEstoque() {
	return finalidadeEstoque;
    }
    
    /**
     * Sets the value of the finalidadeEstoque property.
     * 
     */
    public void setFinalidadeEstoque(String value) {
	this.finalidadeEstoque = value;
    }
    
}