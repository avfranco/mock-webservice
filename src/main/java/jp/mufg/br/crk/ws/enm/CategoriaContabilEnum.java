package jp.mufg.br.crk.ws.enm;

public enum CategoriaContabilEnum {
    
    NEGOCIACAO("Negocia��o", "1"), DISPONIVEL_PARA_VENDA("Dispon�veis para venda", "2"), MANTIDOS_ATE_O_VENCIMENTO("Mantidos at� o vencimento", "3");
    
    private String descricao;
    private String cod;
    
    private CategoriaContabilEnum(String descricao, String cod) {
	this.descricao = descricao;
	this.cod = cod;
    }
    
    public String getDescricao() {
	return descricao;
    }
    
    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }
    
    public String getCod() {
	return cod;
    }
    
    public void setCod(String cod) {
	this.cod = cod;
    }
    
}
