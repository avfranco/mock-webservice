package jp.mufg.br.crk.ws.enm;

public enum FinalidadeEstoqueEnum {
    
    INVESTMENT("", "1"), TRADING("TTRD", "2"), BANKING("BANK", "3"), HEDGE("HDCG", "4"), COMPULSORY("", "5"), DEPOSIT("", "6"), HEDGE_ACCOUNTING("THAC", "7");
    
    private String strategy;
    private String cod;
    
    private FinalidadeEstoqueEnum(String strategy, String cod) {
	this.strategy = strategy;
	this.cod = cod;
    }
    
    @Override
    public String toString() {
	switch (this) {
	    case INVESTMENT:
		return "Investment";
	    case TRADING:
		return "Trading";
	    case BANKING:
		return "Banking";
	    case HEDGE:
		return "Hedge";
	    case COMPULSORY:
		return "Compulsory";
	    case DEPOSIT:
		return "Deposit";
	    case HEDGE_ACCOUNTING:
		return "Hedge Accounting";
	    default:
		return "unrecognizedFinalidadeEstoqueEnum";
	}
    }
    
    public String getStrategy() {
	return strategy;
    }
    
    public void setStrategy(String strategy) {
	this.strategy = strategy;
    }
    
    public String getCod() {
	return cod;
    }
    
    public void setCod(String cod) {
	this.cod = cod;
    }
    
}
