package jp.mufg.br.crk.ws.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jp.mufg.br.crk.ws.enm.FinalidadeEstoqueEnum;

import org.springframework.format.annotation.DateTimeFormat;

public class teste {
    
    @DateTimeFormat(iso = DateTimeFormat.ISO.NONE)
    static Date today = new Date();
    
    /**
     * @param args
     */
    public static void main(String[] args) {
	
	// String output;
	// SimpleDateFormat formatter;
	// Locale ptBr = new Locale("pt", "BR");
	
	// formatter = new SimpleDateFormat("dd-MM-yyyy", ptBr);
	// output = formatter.format(today);
	// output = today.toString();
	// System.out.println("dd-MM-yyyy" + " " + output);
	
	RequestBondsPortfolioVO requestVO = new RequestBondsPortfolioVO();
	
	requestVO.setFinalidadeEstoque("1");
	PosicaoEstrategia posicaoEstrategia = new PosicaoEstrategia();
	
	List<FinalidadeEstoqueEnum> listaEstoque = new ArrayList<>();
	listaEstoque.add(FinalidadeEstoqueEnum.INVESTMENT);
	
	for (FinalidadeEstoqueEnum finalidadeEstoqueEnum : listaEstoque) {
	    if (requestVO.getFinalidadeEstoque().equals(finalidadeEstoqueEnum.getCod())) {
		
		posicaoEstrategia.setFinalidadeEstoque(finalidadeEstoqueEnum.toString());
	    }
	}
	
	System.out.println(posicaoEstrategia.getFinalidadeEstoque());
    }
}
