package jp.mufg.br.crk.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import jp.mufg.br.crk.ws.vo.RequestBondsPortfolioVO;
import jp.mufg.br.crk.ws.vo.ResponseBondsPortfolioVO;

@WebService
public interface CalypsoToCRKInterface {
    
    @WebMethod(action = "requestBondsPortifolio")
    public ResponseBondsPortfolioVO requestBondsPortfolio(@WebParam(name = "GerarPosicaoTitulosPublicosLivres") RequestBondsPortfolioVO requestVO);
}
